.. arq-soft-1 documentation master file, created by
   sphinx-quickstart on Sun Dec 20 13:01:18 2020.
   
   
Welcome to arq-soft-1's documentation!
======================================
 Proyecto final para la asignatura Arquitectura de Software impartida en la UACM.

.. toctree::
    :maxdepth: 2
    :caption: Contents:

    datos/introduccion
    datos/vision-de-la-solucion
    datos/alcance
    datos/contexto-del-sistema
    datos/informacion-adicional
    



    
    



