Contexto del sistema  
=====================

Stakeholders
--------------------------------

+-----------------------+-------------------+-----------------------+---------------+
|                       |                   |                       |               |
| Nombre                | Roles             | Contacto              | Influencia    |
+=======================+===================+=======================+===============+
|                       |                   |                       |               |
| Pepe Grillo           | vendedor          | Ext 4532              | Alta          |
+-----------------------+-------------------+-----------------------+---------------+
|                       |                   |                       |               |
| Pepito                | cliente           |                       | Alta          |
+-----------------------+-------------------+-----------------------+---------------+
|                       |                   |                       |               |
| Mauricio Hernández    | Administración    | Ext 3322              | Media         |
+-----------------------+-------------------+-----------------------+---------------+
|                       |                   |                       |               |
| Goku                  | Desarrollador     | Ext 8976              | Alta          |
+-----------------------+-------------------+-----------------------+---------------+
|                       |                   |                       |               |
| Pikachu               | Analista          | Tel 55-23-11-67-89    | Alta          |
+-----------------------+-------------------+-----------------------+---------------+
 

Historias de usuario
--------------------------------

+-------------------------+---------------------------------------------------------+------------------------------------------------+----------------+
|                         |                                                         |                                                |                |
| Como...                 | Quiero ...                                              | Para ...                                       | Complejidad    |
+=========================+=========================================================+================================================+================+
|                         |                                                         |                                                |                |
| Vendedor                | Tomar las pedidos.                                      | Poder tener mas ventas                         | Media          |
+-------------------------+---------------------------------------------------------+------------------------------------------------+----------------+
|                         |                                                         |                                                |                |
| Cliente                 | Realizar pedidos.                                       | Poder comprar                                  | alta           |
+-------------------------+---------------------------------------------------------+------------------------------------------------+----------------+
|                         |                                                         |                                                |                |
| Cliente                 | Realizar pedidos en cualquier momento                   | Poder comprar                                  | Alta           |
+-------------------------+---------------------------------------------------------+------------------------------------------------+----------------+
|                         |                                                         |                                                |                |
| Administrativo          | Realizar cambios en los productos                       | Tener información exacta                       | Media          |
+-------------------------+---------------------------------------------------------+------------------------------------------------+----------------+
|                         |                                                         |                                                |                |
| Administrativo          |  Realizar consultas de información                      | Bajar los costos de operación.                 | Alta           |
+-------------------------+---------------------------------------------------------+------------------------------------------------+----------------+
|                         |                                                         |                                                |                |
| Vendedor                | Conocer las existencias de producto                     | Para ofrecer productos                         | Media          |
+-------------------------+---------------------------------------------------------+------------------------------------------------+----------------+



Diagrama de contexto
------------------------------

.. image:: diagrama-contexto.png