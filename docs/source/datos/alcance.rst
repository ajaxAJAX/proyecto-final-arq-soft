Alcance
=============
    
Se desarrollara una plataforma web que tenga las funcionalidades de altas, bajas y cambios, la cual será utilizada por el personal de la comercializadora de dulces, donde podrá consultar la información de los productos y realizar pedidos, además contara por una sección que será manejada por el personal administrativo que se encargara de dar mantenimiento a los datos de la aplicación, también se tiene  contemplando desarrollar una tienda en línea, la cual estará sincronizada con la información de la aplicación con la que cuenta  los trabajadores en la comercializadora.

Entregas prevista
--------------------
+----------+------------------------------------------+---------------------------------------+
|          |                                          |                                       |
| Núm.     | Tema principal                           | Id característica                     |
+==========+==========================================+=======================================+
|          |                                          |                                       |
| 1.0      | Diseño de esquema de la base de datos    | CAR-01, CAR-08,                       |
+----------+------------------------------------------+---------------------------------------+
|          |                                          |                                       |
| 2.0      | FRONTEND para trabajadores               | CAR-02, CAR-03,CAR-04,CAR-05          |
+----------+------------------------------------------+---------------------------------------+
|          |                                          |                                       |
| 3.0      | BACKEND para administración              | CAR-01,CAR-02,CAR-03,CAR-04,CAR-11    |
+----------+------------------------------------------+---------------------------------------+
|          |                                          |                                       |
| 4.0      | TIENDA ONLINE para clientes              | CAR-06,CAR-07,CAR-09-CAR-10           |
+----------+------------------------------------------+---------------------------------------+








