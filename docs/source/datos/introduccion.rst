
===========================================
Introducción
===========================================

El siguiente proyecto tiene como finalidad crear una aplicación web, para una comercializadora de dulces, que desea comercializar los productos con los que cuenta actualmente, ya que su inventario se ha incrementado considerablemente,  ha decidido  integrarse a la venta en línea, y considerando que es de vital importancia llevar un control de sus productos para poder ofrecer un servicio inmediato y de calidad lo cual le genere mas ventas.

Contexto del negocio
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Antecedentes
-------------

La problemática principal que la comercializadora de dulces desea solucionar es la desorganización de la información de sus productos, y existencias, lo cual le ha generado pérdida de ventas y compras excesivas en algunos casos, no contar con un catálogo actualizado de productos que todos los trabajadores puedan consultar y alimentar de información, causa retrasos en las operaciones de la comercializadora.


Objetivos de negocio
-------------------------

+----------+---------------------------------------------------------+
|          |                                                         |
| Id       | Objetivos de negocio                                    |
+==========+=========================================================+
|          |                                                         |
| ON-01    | Migrar los datos actuales a una nueva base de dato      |
+----------+---------------------------------------------------------+
|          |                                                         |
| ON-02    | Ingresar productos al sistema en cualquier horario      |
+----------+---------------------------------------------------------+
|          |                                                         |
| ON-03    | Disponer de la información de cada producto             |
+----------+---------------------------------------------------------+
|          |                                                         |
| ON-04    | Consulta total del inventario.                          |
+----------+---------------------------------------------------------+
|          |                                                         |
| ON-05    | Actualización de información de productos.              |
+----------+---------------------------------------------------------+
|          |                                                         |
| ON-06    | Acceso al catálogo de productos en cualquier momento    |
+----------+---------------------------------------------------------+
|          |                                                         |
| ON-07    | Pedidos de clientes en cualquier horario                |
+----------+---------------------------------------------------------+
|          |                                                         |
| ON-08    | Gestión de usuario                                      |
+----------+---------------------------------------------------------+
|          |                                                         |
| ON-09    | Consulta de pedidos                                     |
+----------+---------------------------------------------------------+

